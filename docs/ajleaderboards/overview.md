---
id: overview
title: ajLeaderboards
sidebar_label: Overview
slug: /ajleaderboards/
---

ajLeaderboards is a free plugin for creating simple leaderboards based on placeholderapi placeholders on your server.

If you would like to set up the plugin, head to the [setup page](/ajleaderboards/setup/setup).
