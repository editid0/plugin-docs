---
id: permissions
title: Permissions
sidebar_label: Permissions
---
Here is a list of permissions for the plugin.

| Permission | Description |
| --- | --- |
| ajleaderboards.use | Permission to use commands in /ajleaderboards |
