---
id: placeholders
title: Placeholders
sidebar_label: Placeholders
---
These placeholders are for outputting leaderboards.
Do not try to use them in the ajLeaderboards add command!

If you don't know how to use them, check out [the example on holograms on the setup page](/ajleaderboards/setup/setup#3-optional-display-the-leaderboard-in-a-hologram).

| Placeholder | Description |
| ------ | ------ |
| `%ajleaderboards_board_<board>_<number>_name%` | Shows the name of the player in `<number>` place on leaderboard `<board>` |
| `%ajleaderboards_board_<board>_<number>_value%` | Shows the score of the player in `<number>` place on leaderboard `<board>`  |
| `%ajleaderboards_position_<board>%` | Shows what position the player is at in the leaderboard `<board>`  |
| `%ajleaderboards_board_<board>_<number>_prefix%` | Shows the vault prefix of the player in `<number>` place on leaderboard `<board>` |
| `%ajleaderboards_board_<board>_<number>_suffix%` | Shows the vault suffix of the player in `<number>` place on leaderboard `<board>` |
| `%ajleaderboards_board_<board>_<number>_color%` | Shows the color of the players name. It is basically the prefix but only the color codes. |
