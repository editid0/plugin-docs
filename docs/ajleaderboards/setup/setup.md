---
id: setup
title: Setup
sidebar_label: Setup
---
Here are the steps for setting up the plugin. If you have any questions, you can contact me (preferably on discord. Invite link on the plugin page)

Make sure you have the permision `ajleaderboards.use` to setup the plugin.

## 1. Figure out which placeholder to use
ajLeaderboards tracks tops by looking at what a player would see if they were to view a certain placeholder from a plugin.
Because of this, ajLeaderboards supports thousands of plugins with no extra work for either developers.

You can find a list of placeholders [here](https://placeholderapi.com/placeholders).
This is not a complete list, but it shows all of the built-in papi placeholders and some added by plugins too.

The placeholder you choose much return only a normal number. The number can have commas, but cannot be formatted.

When picking a placeholder to use, make sure it is not already a top placeholder from that plugin.
An easy way to tell this is if the placeholder says "leaderboard" or "top" in it.
Existing top placeholders will not work with ajLeaderboards

In this guide, I will be using the placeholder `%statistic_player_kills%` as an example, which in the end will create a kills leaderboard.

## 2. Add the placeholder to ajLeaderboards

Once you have chosen a placeholder to use, you need to register the placeholder with ajLeaderboards so that we can start recording scores for the leaderboard.

To add a placeholder, use the command `/ajlb add %placeholder%`, and make sure to replace `%placeholder%` with the placeholder you chose in step one.

Using the kills example, I would do the command `/ajlb add %statistic_player_kills%`.

## 3. (optional) Display the leaderboard in a hologram

If you would like, you can display the leaderboard in a hologram.

To show the leaderboard in a hologram, simply use the [PAPI placeholders](/ajleaderboards/setup/placeholders).

Here is an example with MurderMystery wins
(using HolographicDisplays and [HolographicExtension](https://www.spigotmc.org/resources/holographicextension.18461/))

```yaml
mmwinstop:
  location: world, 78.706, 69.000, 57.470
  lines:
  - '&6=== &cMurderMystery&e top wins &6==='
  - '&61.{medium}&e%ajleaderboards_board_murdermystery_wins_1_name% &7- &e%ajleaderboards_board_murdermystery_wins_1_value%'
  - '&62.{medium}&e%ajleaderboards_board_murdermystery_wins_2_name% &7- &e%ajleaderboards_board_murdermystery_wins_2_value%'
  - '&63.{medium}&e%ajleaderboards_board_murdermystery_wins_3_name% &7- &e%ajleaderboards_board_murdermystery_wins_3_value%'
  - '&64.{medium}&e%ajleaderboards_board_murdermystery_wins_4_name% &7- &e%ajleaderboards_board_murdermystery_wins_4_value%'
  - '&65.{medium}&e%ajleaderboards_board_murdermystery_wins_5_name% &7- &e%ajleaderboards_board_murdermystery_wins_5_value%'
  - '&66.{medium}&e%ajleaderboards_board_murdermystery_wins_6_name% &7- &e%ajleaderboards_board_murdermystery_wins_6_value%'
  - '&67.{medium}&e%ajleaderboards_board_murdermystery_wins_7_name% &7- &e%ajleaderboards_board_murdermystery_wins_7_value%'
  - '&68.{medium}&e%ajleaderboards_board_murdermystery_wins_8_name% &7- &e%ajleaderboards_board_murdermystery_wins_8_value%'
  - '&69.{medium}&e%ajleaderboards_board_murdermystery_wins_9_name% &7- &e%ajleaderboards_board_murdermystery_wins_9_value%'
  - '&610.{medium}&e%ajleaderboards_board_murdermystery_wins_10_name% &7- &e%ajleaderboards_board_murdermystery_wins_10_value%'
```

This is what the hologram will look like:
![Hologram Example](../img/hologram_example.png)

If the placeholders don't work for you (show up as just the placeholders) and you are using HolographicDisplays,
make sure you have [HolographicExtension](https://www.spigotmc.org/resources/holographicextension.18461/) and its dependencies (ProtocolLib) installed.

## 4. (optional) Display the leaderboard using signs

You can also display the leaderboard using signs.

To make leaderboard signs, simply place blank signs where you want them to be. (each sign is one person)

To add the signs, use the command `/ajlb signs add <board> <position>`

For example, with the example leaderboard from earlier (statistic_player_kills),
if you wanted to show the player in the #1 position, you would look at the sign and use this command:
`/ajlb signs add statistic_player_kills 1`

## 5. (optional) Display the leaderboard using heads

If you want to show the leaderboard using heads, you can.

Just follow step 4, then place a head above or in front of the sign.

## 6. (optional) Display the leaderboard using armor stands

If you want to show the leaderboard using a head on an armor stands, you can.

Just follow step 4, then place an armor stand above or in front of the sign

## 7. (optional) Display the leaderboard using NPCs

If you want to show the leaderbaords using ncps, you can use [Citizens](https://www.spigotmc.org/resources/citizens.13811/).

Make sure you are running a recent version of Citizens.

Then, simply create an npc. Set the skin to be the ajLeaderboards name placeholder, and the name to the name placeholder and the value. See the example below.

If you want to change the rate that the npcs update, you can with the `placeholder-update-frequency-ticks` option in Citizen's config.

### Example
In this example, I will be creating an NPC that will show the #1 player for the  `statistic_player_kills` leaderboard.

1. Create the npc with the name placeholder and the value. This command will have the nametag show like this: `ajgeiss0702 - 12 kills`
```
/npc create &a%ajleaderboards_board_statistic_player_kills_1_name% &7- &6%ajleaderboards_board_statistic_player_kills_1_value% kills
```
2. Set the skin placeholder.
```
/npc skin %ajleaderboards_board_statistic_player_kills_1_name%
```
