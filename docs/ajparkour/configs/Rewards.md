This is a guide that outlines how to use rewards.yml.

For both intervals and exceptions, only the `commands` and `message` options are required. All of the other options are optional.

For the commands, if you start it with `[p]`, it will execute the command as the player instead of the console. When executing commands, the plugin will also replace `{PLAYER}` with the player's name. If you would like to suggest more placeholders for commands, feel free to join my discord server.


## No rewards
If you would like to set the plugin to have no rewards, you can use this config:
```yaml
# This is the rewards file.
# Here is a guide for this: https://wiki.ajg0702.us/ajparkour/configs/Rewards
intervals: {}
exceptions: {}
specials:
  beat-server-record:
    message: ''
    commands: []
  start:
    message: ''
    commands: []
  end:
    message: ''
    commands: []
```


## Intervals
Here is an example of an interval with all options:
```yaml
'10':
  message: '&aCongrats! &7You got to {SCORE} jumps! &bHave a diamond!'
  areas: 'spawn,shop'
  commands:
  - 'give {PLAYER} diamond 1'
```
This interval will give the player a diamond and a congrats message every 10 jumps (10, 20, 30, 40, etc). To add more commands to the command list, just add another line like this:
```yaml
  - 'broadcast {PLAYER} just got a diamond on ajParkour!'
```
under the previous command. So the finished interval would look something like this:
```yaml
'10':
  message: '&aCongrats! &7You got to {SCORE} jumps! &bHave a diamond!'
  areas: '!minigame'
  commands:
  - 'give {PLAYER} diamond 1'
  - 'broadcast {PLAYER} just got a diamond on ajParkour!'
```


## Exceptions
Here is an example of an exception with all options:
```yaml
'100':
  message: '&a&lCongrats!!! &7You got to &d&l100 &r&7jumps!! Awesome job! Have an emerald!'
  cooldown: 15
  areas: '!minigame'
  first-time-only: false
  commands:
  - give {PLAYER} emerald 1
```
This exception will activate when the player reaches 100 jumps, but only if the exception has not been executed in the past 15 seconds. If `first-time-only` was true, the exception would only be executed if it has not been executed for that player before.

The same rules from exceptions apply for the `commands` option.

## Per-area rewards
If you would like to have a reward only work in certain areas, then you can use the `areas` setting.
You can put one or multiple areas. If you put multiple areas, you need to seperate them with commas.
Example of one area: (this reward will only work in an area named `spawn`)
```yaml
areas: 'spawn'
```
Example of multiple areas: (this reward will only work on the areas called `spawn` and `shop`)
```yaml
areas: 'spawn,shop'
```

### Not operator
If you would like rewards to work in all areas except a few, you can use the `!` operator.
Example: (this reward will work on all areas except one called `minigame`)
```yaml
areas: '!minigame'
```
Example of multiple areas: (this reward will work on all areas except the ones named `minigame` and `minigame-waiting`)
```yaml
areas: '!minigame,!minigame-waiting'
```

## Specials
These rewards are special rewards.

### Beat server record
This reward is activated when a player reaches the server's highscore. The `message` and `commands` work the same here as they do with other rewards.

This is the default:
```yaml
beat-server-record:
  message: '&a&lCongrats!&r&7 You reached the server high score!'
  commands:
  - give {PLAYER} emerald 1
```
### Start
This reward isnt really a reward, but is more an option for you to have a command execute when someone starts parkour. The `message` and `commands` work the same here as they do with other rewards.

This is the default:
```yaml
start:
  message: ''
  commands: []
```
### End
This is exactly the same as the start special reward, except that it happens after the player falls instead of after they start.

This is the default:
```yaml
end:
    message: ''
    commands: []
```

## Conclusion

If you have any questions, please join my discord server to ask questions or suggest features! I usually reply within 30 mins (unless I'm asleep of course).
Here is an example of a complete rewards.yml with the examples above:
```yaml
intervals:
  '10':
    message: '&aCongrats! &7You got to {SCORE} jumps! &bHave a diamond!'
    areas: '!minigame'
    commands:
    - 'give {PLAYER} diamond 1'
exceptions:
  '100':
    message: '&a&lCongrats!!! &7You got to &d&l100 &r&7jumps!! Awesome job! Have an emerald'
    areas: '!minigame'
    cooldown: 15
    commands:
    - 'give {PLAYER} emerald 1'
  '1':
    message: '&a&lCongrats, you made a jump!'
    first-time-only: true
    commands: []
```
