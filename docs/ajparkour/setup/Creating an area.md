Hello and welcome to ajParkour!

I made a video that shows you how to set up an area. You can also read the rest of this guide for more details.

[The tutorial video](https://youtu.be/zW230LaE_lM)

**If you get stuck at any point**, please join my [discord server](https://discord.gg/FWV457K) and I will help you.

## Preparing the area
Before you can start setting up the area, you should make sure that it meets the following requirements:
*  The parkour area is at *least* 15x15x15 blocks. (a larger area is highly recommended)
*  The parkour area is *mostly* clear of other blocks (having it be completely empty of other blocks is recommended)
*  (optional) a place the player will be teleported to when they fall. Otherwise, make sure they are not trapped if they fall.
*  (optional) a place where the player can walk into to start parkour (a portal)

## Creating the area
The first step to start the creation process is the create and name the area. To do this, use the command `/ajParkour setup create <name>`.

Example: `/ajParkour setup create spawn` will create an area named `spawn`.

## Setting the area
Next, you need to set the actual area of the parkour.

To do this, you can use the commands `/ajParkour setup pos1` and `/ajParkour setup pos2`. These commands are similar to worldedit's `//pos1` and `//pos2`

Speaking of worldedit, you can use worldedit instead of the pos1 and pos2 commands. To do this, select the area with worldedit, then use `/ajParkour setup we` to tell ajParkour to use that area.

## Setting the difficulty
Next, you need to set the difficulty of the area. To do this, you use the command `/ajParkour setup difficulty <difficulty>`. Here are the difficulties and how hard they are:

| Difficulty | # of blocks per jump |
| ------ | ------ |
| Easy | 1 |
| Medium | 1-2 |
| Hard | 2-3 |
| Expert | 4-5 |
| Balanced | See below |

### Balanced difficulty
The balanced difficulty will increase the difficulty as the player gets a higher score. Here is a table that shows what difficulty each score is at.

| Score | Difficulty |
| ------ | ------ |
| 0-9 | Easy |
| 10-29 | Medium |
| 30-69 | Hard |
| 70+ | Expert |

This may be configurable in the future, but this is it for now.

## (optional) Setting a fall position
If you would like the plugin to teleport the player somewhere when they fall (like near a start portal), you can set the fallpos.

To set this, you stand where you would like the player to be teleported and type `/ajParkour setup fallpos`.

## Saving the area
First, you should double-check that you have set all of the required positions by typing `/ajParkour setup info`. If you see anything that is red, you will need to go back and set that.

If you do not see any red in the info message, you can go ahead and save the area by typing `/ajParkour setup save`. This will save the area to a file and enable it.

Your area is now ready for use! You can parkour on it by typing `/ajParkour start <area>` (replace `<area>` with what you named your area)

If you want to set a start position (portal), see the section below.

## Setting a portal for your area (start position)
If you want to make a place your players can walk into to start parkour, you should make a portal.

To make a portal, you use the command `/ajParkour portals create <name> [area]`. Replace `<name>` with what you want to name the portal, and `[area]` with what area you want the portal to go to.

If you do not put an area, the portal will do the same thing as `/ajParkour start`. If you do set an area, it will do the same thing as `/ajParkour start <area>`.
