---
id: placeholders
title: Placeholders
sidebar_label: Placeholders
---

These are the PlaceholderAPI placeholders for ajParkour.
These placeholders will work anywhere were PlaceholderAPI placeholders are supported
(which should be in most plugins).
Make sure to not include the `<` and `>` in the placeholders!

| Placeholder | Description |
| ------ | ------ |
| `%ajpk_stats_top_name_<number>%` | Returns the name of the person in top position number `<number>`. |
| `%ajpk_stats_top_name_<number>_<area>%` | Returns the name of the person in top position number `<number>` for area `<area>`. |
| `%ajpk_stats_top_score_<number>%` | Returns the score of the person in top position number `<number>`. |
| `%ajpk_stats_top_score_<number>_<area>%` | Returns the score of the person in top position number `<number>` for area `<area>`. |
| `%ajpk_stats_top_time_<number>%` | Returns the time that the player in top position number `<number>` took to complete the parkour
| `%ajpk_stats_highscore%` | Returns the player's high score. |
| `%ajpk_stats_highscore_<area>%` | Returns the player's high score in the area `<area>` |
| `%ajpk_current%` | Returns the player's current score. |
| `%ajpk_jumping%` | Returns the number of players in parkour. |
