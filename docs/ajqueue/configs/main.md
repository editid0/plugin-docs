---
title: Main config
sidebar_label: Main config
---

This is the default main config. Everything is explained in the comments.
```yaml
# Dont touch this number please
config-version: 20

# The time the server will wait between sending people in the queue
#  Default: 5
wait-time: 5

# The time that a server has to be offline to be marked as offline instead of restarting
#  Default: 120
offline-time: 120

# The time the server will wait between sending players update messages on what position they are in the queue.
# Default: 10
message-time: 10

# If a player is in a server, you can have the plugin make them automatically join a queue for another server
# Example with the default values: Player joins survivalqueue server, they will auto-join the queue for survival
queue-servers:
- 'survivalqueue:survival'

# Should the plugin send an actionbar to the player?
# The actionbar contains some info such as which server they are queued for, what position they are in, estimated time remaining, etc.
send-actionbar: true

# What kick reasons should cause the player to be removed from the queue?
# For example, if one of the below kick reasons is 'banned' and the player gets kicked when trying to connect to
#  a server in a queue with a message saying "You are banned from this server!" then it will kick them from the queue too.
kick-reasons:
- 'banned'
- 'blacklisted'


# Should we remove a player from the queue if they move servers?
# If they join another queue, they will be removed from the previous one no matter what
# This is more meant for if you have multiple lobbies if you want to let the player switch
# between them without losing their queue position
#  Default: false
remove-player-on-server-switch: false


# Should we wait until the server is done loading to load the servers?
# Enable this if you have a plugin that adds servers to the server list during startup.
#  Default: false
wait-to-load-servers: false

# How long should we wait after the server finishes loading to load the server list?
# Only works if the above is enabled.
# This is in miliseconds so 1000 = 1 second
#  Default: 500
wait-to-load-servers-delay: 500


# How often (in seconds) we should check for new servers to add queues for.
# If you dynamicly add servers, set this to something other than 0.
# To disable, set to 0
reload-servers-interval: 0


# Should we require permissions for players to be able to join queues?
# If enabled, players will be required to have the permission ajqueue.queue.<server>
#  Default: false
require-permission: false


# Should we let players join more than one queue?
# If enabled, players will be able to be in multiple queues at once.
#  Default: true
allow-multiple-queues: true

# If the player is queued for multiple servers, which server should we pick to use in things like placeholders and actionbars
# Options are first and last
#  Default: last
multi-server-queue-pick: last


# THIS FEATURE IS ONLY AVAILABLE ON ajQueuePlus (https://www.spigotmc.org/resources/ajqueueplus.79123/)
# This will show players a different name than the actual bungeecord server name
# for example, instead of showing players "event-a", this option can make it appear as "Event A"
# With this example, you would use this: - "event-a:Event A"
# Note that currently players still have to use the normal names in queue commands and leave commands
#  Format: "realname:Alias"
server-aliases:
- "event-a:Event A"


# How long should we wait after a server is online before sending players?
# The server will still show up as offline or restarting until this amount of time after its up
# Meant to let your server 'cool down' after lag from starting up
# In seconds
#  Default: 1
wait-after-online: 1


# This is for helping with finding issues with the server pinged
# This will spam the console when enabled
# When this enabled, if servers are offline then it will spam errors. You can ignore them.
#  Default: false
pinger-debug: false


# When a queue is paused, should we prevent players from joining it?
#  Default: false
prevent-joining-paused: false

# When a server goes back online, should we send all players in the queue instantly?
# Default: false
send-all-when-back-online: false

# Require a permission for players to be able to join a queue from a server
# If enabled, players will need the permission ajqueue.joinfrom.<server> to join queues.
# Replace <server> with the name of the server
#  Default: false
joinfrom-server-permission: false

# Server groups are a group of servers that you can queue for. It will send you to the server that is the least full.
# If all servers in the group are full, it will act the same as it would when a single server is full.
# Same if all servers are offline. It will only send players to servers that are online.
#  Format: "groupname:server1,server2,etc"
server-groups:
- "lobbys:lobby-1,lobby-2,lobby-3"


# Should we allow tab-completing in the /queue command?
#  Default: true
tab-complete-queues: true

# Should we have no wait time for these servers?
# If the server is joinable, the plugin will attempt to send players who join these queues as soon as they join.
# If the server is not immidiatly joinable, they will have to wait for the normal wait-time
# This also works with group
# NOTE: Server names are caps sensitive
send-instantly:
- "lobbys"

# Should we log to the bungeecord console when a player fails to get sent to a server from the queue?
# Enable this if you are having an issue with one player stopping the queue
#  Default: false
send-fail-debug: false

# After how many (unsuccessfull) attempts of sending the player should we remove them from the queue?
# Set to -1 to disable
#  Default: 10
max-tries: 10

# Should we enable the ajqueue.bypasspaused permission?
# If enabled, anyone with the permission ajqueue.bypasspaused will be able to join paused servers
#  Default: false
enable-bypasspaused-permission: false


# Should we check to make sure that people dont get sent quicker than wait-time?
#  Default: true
check-last-player-sent-time: true

# Should we send debug info to the console when priority queue is used?
# This will print several lines to the console when a player joins a queue,
# so you should probably only use this for debugging purposes
#  Default: false
priority-queue-debug: false


```
