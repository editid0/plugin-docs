---
title: Spigot config
sidebar_label: Spigot config
---
This is the config for the spigot-side. There is only one option.

You do not need to copy the bungeecord config here. Anything in the bungee config that would effect the spigot server is taken from the bungee config automatically.

```yaml
# This is the config for the spigot side.
# You can find more settings in the config of bungee.

# Should we send queue requests from commands in batches?
# Enable this if you have issues with players sometimes not executing commands correctly
# Note though that it could delay queue commands by up to 1 second!
send-queue-commands-in-batches: false
```
