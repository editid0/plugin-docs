---
id: overview
title: ajQueue
sidebar_label: Overview
slug: /ajqueue/
---

ajQueue is a queue plugin that was made because all of the queue plugins I tried were either very buggy and had a weird config, or were over-priced for what they offered.
