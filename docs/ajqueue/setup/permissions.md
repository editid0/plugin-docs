---
title: Permissions
sidebar_label: Permissions
---
These are the permissions for the plugin. All commands in the commands column are separated by commas.

**NOTE:** All permissions must be given on bungeecord

| Permission | Description | Command(s) |
| --- | --- | --- |
| ajqueue.joinfull | The plugin will still attempt to send you to full servers. Requires another plugin on the target server that will let you join when its full (e.g. Essentials). |  |
| ajqueue.reload | Allows you to reload the config. | /ajqueue reload |
| ajqueue.pause | Allows you to pause servers. | /ajqueue pause |
| ajqueue.send | Allows you to send people to queues. | /ajqueue send |
| ajqueue.list | Allows you to list the queues and the people in them | /ajqueue list |
| ajqueue.bypass | Bypasses the queue and attempts to send you immediately.  If you are not able to join, you will be put in the first position in the queue. (**ajQueuePlus only!**) |
| ajqueue.bypasspaused | Allows you to bypass paused queues. Must be enabled in the config. |
| ajqueue.listqueues | Allows you to list the queues (more user-friendly command than /ajqueue list) | /listqueues |
