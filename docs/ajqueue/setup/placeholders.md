---
title: Placeholders
sidebar_label: Placeholders
---
These are the placeholderapi placeholder for displaying information.

To use these placeholderapi placeholders, ajQueue must also be installed on the server you want to use them. Just put the same jar in the spigot plugins folder, and it should work.

| Placeholder | Description |
| --- | -- |
| `%ajqueue_queued%` | Shows the name of the server the player is queued for |
| `%ajqueue_position%` | Shows the player's position in the queue |
| `%ajqueue_of%` | Shows the number of players in the queue the player is currently in |
| `%ajqueue_inqueue%` | Returns `true` if the player is in a queue, or `false` if they arent |
| `%ajqueue_queuedfor_<server>%` | Returns the number of people that are queued for `<server>` |
