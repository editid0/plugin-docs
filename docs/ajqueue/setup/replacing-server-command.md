---
title: Replacing server command
sidebar_label: Replacing server command
---
It is fairly simple to replace bungeecrd's default /server command with the queue command.


1. Open modules.yml (in the bungeecord server folder)
2. Remove the line that says 'jenkins://cmd_server'
3. Save and close modules.yml
4. Delete cmd_server.jar in the modules folder
5. Restart bungeecord


To undo this, just remove the modules.yml file and restart the server.
