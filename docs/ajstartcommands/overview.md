---
id: overview
title: ajStartCommands
sidebar_label: Overview
slug: /ajstartcommands/
---
ajStartCommands is a plugin that will run commands when the server starts, or when players join.
