module.exports = {
  ajparkour: [
    {
      type: 'category',
      label: 'ajParkour',
      items: [
        'ajparkour/Home',
        {
          "Setup": [
            'ajparkour/setup/Creating an area',
            'ajparkour/setup/Permissions',
            'ajparkour/setup/placeholders'
          ]
        },
        {
          "Configs": [
            'ajparkour/configs/main',
            'ajparkour/configs/Rewards',
            'ajparkour/configs/Blocks',
            'ajparkour/configs/jumps'
          ]
        },
        'ajparkour/Upgrading from v1 to v2',
        {
          "API": [
            'ajparkour/api/Getting-Started'
          ]
        }
      ]
    }
  ],
  ajleaderboards: [
    {
      type: 'category',
      label: 'ajLeaderboards',
      items: [
        'ajleaderboards/overview',
        {
          "Setup": [
            'ajleaderboards/setup/setup',
            'ajleaderboards/setup/placeholders',
            'ajleaderboards/setup/permissions'
          ]
        },
        {
          "Configs": [
            'ajleaderboards/configs/main'
          ]
        }
      ]
    }
  ],
  ajqueue: [
    {
      type: 'category',
      label: 'ajQueue',
      items: [
        'ajqueue/overview',
        {
          "Setup": [
            'ajqueue/setup/permissions',
            'ajqueue/setup/placeholders',
            'ajqueue/setup/replacing-server-command'
          ]
        },
        {
          "Configs": [
            'ajqueue/configs/main',
            'ajqueue/configs/spigot'
          ]
        },
        'ajqueue/priority-explanation'
      ]
    }
  ],
  ajstartcommands: [
    {
      type: 'category',
      label: 'ajStartCommands',
      items: [
        'ajstartcommands/overview',
        'ajstartcommands/permissions',
        'ajstartcommands/flags'
      ]
    }
  ],
  ajtntrun: [
    {
      type: 'category',
      label: 'ajTNTRun',
      items: [
        'ajtntrun/overview',
        {
          "Setup": [
            'ajtntrun/setup/creating-an-arena',
            'ajtntrun/setup/setting-a-lobby',
            'ajtntrun/setup/creating-signs',
            'ajtntrun/setup/arena-requirements',
            'ajtntrun/setup/commands',
            'ajtntrun/setup/placeholders'
          ]
        },
        'ajtntrun/bungee'
      ]
    }
  ]
};
